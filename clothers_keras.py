import os
import joblib
import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier

from flask import Flask, request, flash, jsonify, abort, redirect, url_for, render_template, send_file, after_this_request
from flask_wtf import FlaskForm
from markupsafe import escape
from wtforms import StringField, FileField
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename

import tensorflow as tf
from tensorflow import keras

app = Flask(__name__)

# Using a development configuration
app.config.from_object('config.DevConfig')

# load
new_model = keras.models.load_model('model/clothers_model.h5')

class MyForm(FlaskForm):
    file = FileField()

@app.route('/')
def clear_format():
    """Clear excel format"""
    form = MyForm()
    if form.validate_on_submit():
        try:
            uploaded_file = form.file.data

            [file_handle, filename] = clead_format_function(uploaded_file)

            return send_file(file_handle, mimetype='text/xlsx', attachment_filename=filename, as_attachment=True)
        except Exception:
            return redirect(url_for('bad_request'))

    return render_template('paint_picture.html', form=form)

@app.route('/load_paint', methods = ['POST'])
def load_paint():
    """Return answer"""
    try:
        content = request.get_json()

        class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat','Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

        npa = np.array(content['array'])

        img = npa

        img = rebin(img,(28,28))

        img = (np.expand_dims(img,0))

        predictions_single = new_model.predict(img)

        # to dataframe
        df = pd.DataFrame(predictions_single, columns = class_names)

        # transpose
        df = df.T
        df['name'] = df.index

        # sorting
        df = df.sort_values(by=[0], ascending=False)

        # change to procent
        df[0] = df[0] * 100
        df[0] = df[0].round(3)

        predict = df.to_numpy().tolist()

    except Exception as e:  
        print(e)      
        return redirect(url_for('bad_request'))

    return jsonify(predict)

def rebin(a, shape):
    sh = shape[0],a.shape[0]//shape[0],shape[1],a.shape[1]//shape[1]
    return a.reshape(sh).mean(-1).mean(1)

@app.route('/badrequest')
def bad_request():
    return abort(400)
