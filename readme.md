# Introduction

Hello, example work with drawing picture recognition in flask-microservice app

# Run docker

### build and run
docker-compose up --build

# Request

### upload file in interface
http://localhost:5001/ - draw the picture

### upload file by request
http://localhost:5001/load_paint - can classifire by post request

# Example

![Tux, the Linux mascot](/static/images/Clothing classifier - example.jpg)

# Model
model created by tutorial https://www.tensorflow.org/tutorials/keras/classification?hl=ru